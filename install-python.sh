#!/usr/bin/env bash

os="$(grep -i ^ID= /etc/os-release | grep -ioE "centos|ubuntu" | tr [:upper:] [:lower:])"

# PYTHON  ====================================================================

source ~/.pyenv_loader
if ! command -v pyenv
then
  case $os in
    'ubuntu')
      sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils
      echo '[ -f ~/.pyenv_loader ] && source ~/.pyenv_loader' >> ~/.bashrc
      ;;
    'centos')
      sudo yum install -y zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel
      echo '[ -f ~/.pyenv_loader ] && source ~/.pyenv_loader' >> ~/.bash_profile
      ;;
  esac

  curl -sSL https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash

  echo 'export PYENV_ROOT="$HOME/.pyenv"'       >  ~/.pyenv_loader
  echo 'export PATH="$PYENV_ROOT/bin:$PATH"'    >> ~/.pyenv_loader
  echo 'eval "$(pyenv init -)"'                 >> ~/.pyenv_loader
  source ~/.pyenv_loader

  pyenv install 2.7.12
  which python
  python -V

fi
