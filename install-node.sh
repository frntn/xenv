#!/usr/bin/env bash

os="$(grep -i ^ID= /etc/os-release | grep -ioE "centos|ubuntu" | tr [:upper:] [:lower:])"

# NODE  =====================================================================

source ~/.nvm_loader
if ! command -v nvm

  # Install NVM
  curl -o- https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
  
  case $os in
    'ubuntu') DETECTED_PROFILE="$HOME/.bashrc";;
    'centos') DETECTED_PROFILE="$HOME/.bash_profile";;
  esac
  
  grep NVM_DIR $DETECTED_PROFILE > ~/.nvm_loader
  sed -i '/NVM_DIR/d' $DETECTED_PROFILE
  echo '[ -f ~/.nvm_loader ] && source ~/.nvm_loader' >> $DETECTED_PROFILE

fi

# Install NODE (latest LTS version) + NPM
nvm install --lts

# Update NPM
npm install -g npm

: <<DESC

nvm uses .nvm/ for its own setup
Ultimately nvm uses .nvmrc to define node version to use (like .python-version with pyenv, .rub-version with rbenv)

npm uses .npmrc for its own setup
Ultimately npm will uses package.json (like requirements.txt for python, Gemfile for ruby)

DESC
