#!/usr/bin/env bash

os="$(grep -i ^ID= /etc/os-release | grep -ioE "centos|ubuntu" | tr [:upper:] [:lower:])"

# PYTHON  ====================================================================

source ~/.goenv_loader
if ! command -v goenv
then
  case $os in
    'ubuntu')
      sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils
      echo '[ -f ~/.goenv_loader ] && source ~/.goenv_loader' >> ~/.bashrc
      ;;
    'centos')
      sudo yum install -y zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel
      echo '[ -f ~/.goenv_loader ] && source ~/.goenv_loader' >> ~/.bash_profile
      ;;
  esac
 
  git clone https://github.com/syndbg/goenv.git ~/.goenv

  echo 'export GOENV_ROOT="$HOME/.goenv"'    >  ~/.goenv_loader
  echo 'export PATH="$GOENV_ROOT/bin:$PATH"' >> ~/.goenv_loader
  echo 'eval "$(goenv init -)"'              >> ~/.goenv_loader
  source ~/.goenv_loader

  goenv install 1.6.3
  which go
  go version

fi
