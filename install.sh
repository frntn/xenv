#!/usr/bin/env bash

while [ $# -ne 0 ]
do
  case "$1" in
    'golang'|'python'|'ruby'|'node') ./"install-${1}.sh"
  esac
  shift
done
