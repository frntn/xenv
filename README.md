# frntn/xenv

Helper scripts to install environment managers for :

- Ruby (`rbenv`)
- Golang (`goenv`)
- Python (`pyenv`)
- Node (`nvm`)

These scripts are public but were written for my own personal usage.
Use at your own risks.

```
# Install all
./install.sh golang python ruby node
```
