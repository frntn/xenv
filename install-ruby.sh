#!/usr/bin/env bash

os="$(grep -i ^ID= /etc/os-release | grep -ioE "centos|ubuntu" | tr [:upper:] [:lower:])"

# RUBY  =====================================================================

source ~/.rbenv_loader
if ! command -v rbenv
then
  case $os in
    'ubuntu')
      sudo apt-get install -y libssl-dev libreadline-dev zlib1g-dev
      echo '[ -f ~/.rbenv_loader ] && source ~/.rbenv_loader' >> ~/.bashrc
      ;;
    'centos')
      sudo yum install -y openssl-devel readline-devel zlib-devel
      echo '[ -f ~/.rbenv_loader ] && source ~/.rbenv_loader' >> ~/.bash_profile
      ;;
  esac

  rm -rf ~/.rbenv ~/.rbenv_loader
  git clone https://github.com/rbenv/rbenv.git      ~/.rbenv
  git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
  cd ~/.rbenv
  src/configure
  make -C src

  echo 'export RBENV_ROOT="$HOME/.rbenv"'       >  ~/.rbenv_loader
  echo 'export PATH="$HOME/.rbenv/bin:$PATH"'   >> ~/.rbenv_loader
  echo 'eval "$(rbenv init -)"'                 >> ~/.rbenv_loader
  source ~/.rbenv_loader

  # install ruby + rubygems + bundler

  rbenv install 1.9.3-p551
  rbenv shell 1.9.3-p551
  gem install bundler

  rbenv install 2.3.1
  rbenv shell 2.3.1
  gem install bundler

  ruby -v

fi
